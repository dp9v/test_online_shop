/**
 * Created by d.Polkovnikov on 25.08.2015.
 */
$(document).ready(function () {
    $('#sign_in_form').submit(sign_in);
    $('.good_count').change(count_change);
    $(document).on('click', '.add_to_cart', add_to_cart);
})

function sign_in(event) {
    event.preventDefault();
    form = $(this);
    url = event.currentTarget.action;
    data = form.serialize();
    $.post(url, data, success, 'json').fail(error);

    return false;

    function success(data) {
        if (data.res)
            location.reload();
        else
            alert("Ошибка авторизации");
    }

    function error() {
        alert("Ошибка работы сервера");
    }
}

function add_to_cart(e) {
    btn = $(this);
    id = $(this).data('good_id');
    token = $('input[name=csrfmiddlewaretoken]').first().val();
    url = $('#add_to_cart_url').val();
    data = {'csrfmiddlewaretoken': token, 'good_id': id};
    $.post(url, data, success, 'json').fail(error);

    function success(data) {
        if (data.res) {
            $('#cart_count').html(data.count + "");
            btn.addClass("disabled");
        } else {
            alert("Ошибка добавления");
        }
    }

    function error() {
        alert("Ошибка работы сервера");
    }
}

function count_change(e) {
    input = $(this);
    token = $('input[name=csrfmiddlewaretoken]').first().val();
    id = input.data('id');
    count = input.val();
    url = "/client/change_count/";
    data = {'id': id, 'count': count, 'csrfmiddlewaretoken': token};
    if (id>0 && count>0)
        $.post(url, data, success, 'json')

    function success(data) {
        if (data.res) {
            input.parent().children(".res_good_total_price").html(data.price + ' руб.');
            $('.res_total_price').html(data.total + ' руб.');
        } else {
            alert("Ошибка входных данных")
        }
    }
}


/*function make_order(event) {
    event.preventDefault();
    items = [];
    $("input.good_count").each(function (i) {
        data = $(this);
        id = data.data("id");
        count = parseInt(data.val());
        items.push({'id': id, 'count': count});
    });
    token = $('input[name=csrfmiddlewaretoken]').first().val();
    url = event.currentTarget.action;
    data = {csrfmiddlewaretoken: token, goods: ['1', '2']};

    $.post(url, data, success, 'json').fail(error);

    function success(data) {
        alert(data);
    }

    function error() {
        alert("Ошибка работы сервера");
    }
}*/
