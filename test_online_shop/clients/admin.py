import csv
from django.http import HttpResponse
from django.contrib import admin
from clients.models import Order, OrderGood


def export_to_csv(modeladmin, request, queryset):
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="export.csv"'

	writer=csv.writer(response, delimiter=";")
	for item in queryset:
		writer.writerow([item.id, item.user_name(), item.get_total()])
	return response

export_to_csv.short_description = "Export to csv"


class OrderGoodInline(admin.TabularInline):
	model = OrderGood


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
	inlines = (OrderGoodInline,)
	actions = [export_to_csv, ]
