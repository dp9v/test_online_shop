from random import choice
from django.db import models
from django.contrib.auth.models import User
from catalog.models import Good

# Create your models here.
class Client(models.Model):
	user = models.OneToOneField(User)
	token = models.CharField(blank=True, max_length=50,
							 default="".join(choice("abcdefghigklmnopqstuvwxyz1234567890") for i in range(50)))
	time = models.DateTimeField(auto_now_add=True)

	@classmethod
	def activate(cls, token):
		try:
			object = cls.objects.get(token=token)
			if object.user.is_active:
				return False
			else:
				object.user.is_active = True
				object.user.save()
				object.token = ""
				object.save()
				return True
		except:
			return False

	@classmethod
	def create_user(cls, email, password):
		user = User.objects.create_user(username=email, email=email, password=password)
		user.is_active = False
		user.save()
		client = cls(user=user)
		client.save()
		return client


class Order(models.Model):
	user = models.ForeignKey(User, verbose_name='Пользователь', blank=True, null=True)
	goods = models.ManyToManyField(Good, through='OrderGood', verbose_name='Товары', blank=True)
	is_finish = models.BooleanField(default=False, verbose_name='Заказ закрыт')

	def __str__(self):
		res = self.user_name() + ' - ' + str(self.goods.all().count())
		if self.is_finish:
			res += ' - Завершено'
		return res

	@classmethod
	def get_order(cls, request):
		user = request.user
		if request.user.is_anonymous():
			if 'order' in request.session:
				order = cls.objects.get(id=request.session.get('order'))
			else:
				order = Order.objects.create()
				request.session['order'] = order.id
		else:
			try:
				order = cls.objects.get(user=user, is_finish=False)
			except:
				order = Order.objects.create(user=user)
		return order

	def goods_count(self):
		return self.goods.all().count()

	def add_good(self, good_id):
		if OrderGood.objects.filter(good__id=good_id, order=self).count() != 0:
			return
		good = Good.objects.get(id=good_id)
		OrderGood.objects.create(good=good, order=self)

	def get_total(self):
		order_goods = OrderGood.objects.filter(order=self)
		total = 0
		for order_good in order_goods:
			total += order_good.count * order_good.good.price
		return total

	def close(self):
		# тут должна быть более сложная логика. Как минимум должны высылаться письма пользователю и владельцу сайта.
		self.is_finish = True
		self.save()

	def user_name(self):
		if self.user:
			return self.user.username
		else:
			return "Пользователь не указан"

	class Meta:
		verbose_name = 'Заказ'
		verbose_name_plural = 'Заказы'


class OrderGood(models.Model):
	good = models.ForeignKey(Good, verbose_name='Товар')
	order = models.ForeignKey(Order, verbose_name='Заказ')
	count = models.IntegerField('Количество', default=1)
