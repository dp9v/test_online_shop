from django.shortcuts import render, HttpResponse, redirect
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from clients.forms import RegisterForm
from clients.models import Client, Order, OrderGood
from django.contrib import auth
import json


class RegisterView(FormView):
	form_class = RegisterForm
	template_name = "register.html"

	def form_valid(self, form):
		user = Client.create_user(form.cleaned_data['email'], form.cleaned_data['password'])
		return render(self.request, self.template_name, {'created_user': user})


class CartView(ListView):
	model = OrderGood
	context_object_name = 'goods'
	template_name = "cart.html"

	def get_queryset(self, queryset=None):
		order = Order.get_order(self.request)
		return OrderGood.objects.filter(order=order).distinct()

	def get_context_data(self, **kwargs):
		context = super(CartView, self).get_context_data(**kwargs)
		total = 0
		for good in context['goods']:
			total += good.count * good.good.price
		context['total'] = total
		return context

	def post(self, request, **kwargs):
		if request.user.is_anonymous():
			return redirect('register')
		self.object_list = self.get_queryset()
		context = self.get_context_data()
		order = Order.get_order(request)
		order.close()
		context['closed']=True
		return render(request, self.template_name, context)


def email_confirm(request, token):
	res = Client.activate(token)
	return render(request, "confirm.html", {'res': res})


def login(request):
	order_anon = Order.get_order(request)
	email = request.POST.get('email')
	password = request.POST.get('password')
	user = auth.authenticate(username=email, password=password)
	if user is not None and user.is_active:
		auth.login(request, user)
		order = Order.get_order(request)
		if order.goods_count() == 0:
			order.delete()
			order_anon.user = user
			order_anon.save()
		else:
			order_anon.delete()
			del request.session['order']
		return HttpResponse(json.dumps({'res': True}), content_type="application/json")
	else:
		return HttpResponse(json.dumps({'res': False}), content_type="application/json")


def add_to_cart(request):
	try:
		id = request.POST.get('good_id')
		cart = Order.get_order(request)
		cart.add_good(id)
		res = {'res': True, 'count': cart.goods_count()}
	except:
		res = {'res': False}
	return HttpResponse(json.dumps(res), content_type='application/json')


def change_count(request):
	order_good = OrderGood.objects.get(id=int(request.POST.get('id')))
	price = 0
	try:
		count = int(request.POST.get('count', 1))
		order_good.count = count
		order_good.save()
		price = order_good.good.price * count
	except:
		return HttpResponse(json.dumps({'res': False}), content_type='application/json')
	order = Order.get_order(request)
	total = order.get_total()
	return HttpResponse(json.dumps({'res': True, 'total': total, 'price': price}), content_type='application/json')
