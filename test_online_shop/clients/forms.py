from django import forms
from django.contrib.auth.models import User

class RegisterForm(forms.Form):
	email = forms.EmailField(widget=forms.EmailInput(attrs={"class": "form-control", "placeholder": "Email address",
															"required": "", "autofocus": ""}))
	password = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={"class": "form-control",
																				"placeholder": "Password",
																				"required": ""}))

	def clean_email(self):
		email = self.cleaned_data['email']
		if User.objects.filter(username = email).count()!=0:
			raise forms.ValidationError("User already exists")
		return email
