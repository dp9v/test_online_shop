from django import template
from clients.models import Order

register = template.Library()


@register.inclusion_tag("blocks/user_menu.html", takes_context=True)
def render_user_menu(context):
	user = context.request.user
	order = Order.get_order(context.request)
	return {'auth': user.is_authenticated(), 'user': user, 'order': order}
