from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from clients.views import RegisterView, CartView, email_confirm, login, add_to_cart,change_count
from django.contrib.auth.views import logout

urlpatterns = [
	#    url(r'^$', CatalogList.as_view(), name='home'),
	url(r'register/$', RegisterView.as_view(), name='register'),
	url(r'confirm/(?P<token>\w+)$', email_confirm, name='confirm'),
	url(r'login/$', login, name='login'),
	url(r'logout/$', logout, name='logout'),
	url(r'add_t_cart/$', add_to_cart, name='add_to_cart'),
	url(r'cart/$', CartView.as_view(), name='cart'),
	url(r'change_count/$', change_count, name='change_count')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
