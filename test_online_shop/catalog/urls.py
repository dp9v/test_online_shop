from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from catalog.views import CatalogList, GoodList, get_tags
from catalog.models import Good
from django.views.generic import DetailView

urlpatterns = [
	url(r'^$', CatalogList.as_view(), name='home'),
	url(r'^category/(?P<slug>[-|\w]+)$', GoodList.as_view(), name='category'),
	url(r'^good/(?P<pk>\d+)$', DetailView.as_view(template_name='good.html', model=Good, context_object_name="good"),
		name='good'),
	url(r'^tags/$', get_tags, name='tags'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
