from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from catalog.models import Category, Good, Tag
import json


class CatalogList(ListView):
	model = Category
	template_name = "home.html"
	context_object_name = "categories"

	def get_queryset(self):
		qs = self.model.objects.filter(is_active=True)
		return qs


class GoodList(ListView):
	model = Good
	template_name = "category.html"
	context_object_name = "goods"
	paginate_by = 16

	def get_queryset(self):
		category = self.kwargs['slug']
		tags = self.request.GET.getlist('tags[]', [])
		qs = self.model.objects.filter(category__machine_id=category, is_active=True)
		if tags:
			tags = Tag.objects.filter(title__in=tags)
			for tag in tags:
				qs = qs.filter(tags__in=[tag, ])
		return qs.distinct()

	def get_context_data(self, **kwargs):
		context = super(GoodList, self).get_context_data(**kwargs)
		context["category"] = get_object_or_404(Category, machine_id=self.kwargs['slug'])
		goods = context['goods']
		goods_2 = []
		for good in goods:
			goods_2.append(good.as_structure(self.request))
		context['goods'] = goods_2
		return context

	def get(self, request, slug):
		self.object_list = self.get_queryset()
		context = self.get_context_data()
		if not request.is_ajax():
			return self.render_to_response(context)
		else:
			data = {'count': context['paginator'].num_pages,
					'now': context['page_obj'].number,
					'goods': context['goods']}
			return HttpResponse(json.dumps(data), content_type="application/json")


def get_tags(request):
	tags = list(Tag.objects.filter(goods__isnull=False).distinct().values_list('title', flat=True))
	return HttpResponse(json.dumps(tags), content_type="application/json")

