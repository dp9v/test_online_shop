from django.db import models
from sorl.thumbnail import get_thumbnail
import clients
#from clients.models import Order


class Category(models.Model):
	title = models.CharField("Имя категории", max_length=50)
	machine_id = models.SlugField('Машиночитаемое имя', max_length=50, unique=True)
	is_active = models.BooleanField('Отображать', default=True)

	@models.permalink
	def get_absolute_url(self):
		return ('category', (), {'slug': self.machine_id})

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = 'Категория'
		verbose_name_plural = 'Категории'


class Good(models.Model):
	title = models.CharField('Наименование товара', max_length=100)
	description = models.TextField('Описание товара', blank=True)
	category = models.ForeignKey(Category, verbose_name='Категория', related_name='goods', on_delete=models.PROTECT)
	image = models.ImageField('Изображение', upload_to='goods', blank=True, null=True)
	tags = models.ManyToManyField('Tag', verbose_name='Тэги', blank=True, related_name='goods')
	properties = models.ManyToManyField('Property', verbose_name='Параметры', blank=True, related_name='goods')
	price = models.IntegerField('Цена', default=0)
	is_active = models.BooleanField('Отображать', default=True)

	def as_structure(self, request):
		cart = clients.models.Order.get_order(request)
		if self.image:
			im_url = get_thumbnail(self.image, '250x250', crop="center").url
		else:
			im_url = "http://placehold.it/250x250"
		data = {'id': self.id,
				'title': self.title,
				'description': self.description,
				'price': self.price,
				'img_url': im_url,
				'url': self.get_absolute_url(),
				'in_cart': self in cart.goods.all()}
		return data

	@models.permalink
	def get_absolute_url(self):
		return ('good', (), {'pk': self.id})

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = 'Товар'
		verbose_name_plural = 'Товары'


class Tag(models.Model):
	title = models.CharField('Название тэга', max_length=50)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = 'Тэг'
		verbose_name_plural = 'Тэги'


class Property(models.Model):
	title = models.CharField('Название свойства', max_length=100)
	value = models.CharField('Значение', max_length=100)
	sort_by = models.IntegerField('Поле для сортировки')

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = 'Свойство товара'
		verbose_name_plural = 'Свойства товаров'

# Create your models here.
