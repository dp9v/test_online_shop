/**
 * Created by d.Polkovnikov on 22.08.2015.
 */
var pages = {
    total: 1,
    page: 1,
    maxVisible: 50
};

$(document).ready(function () {
    init_paginator();
    init_tags();
});

function init_paginator() {
    pages.total = parseInt($('#page-selection').data("count"));
    pages.page = parseInt($('#page-selection').data("now"));
    $('#page-selection').bootpag(pages).on('page', function (event, num) {
        pages.page = num;
        refresh_catalog();
    });
}
function init_tags() {
    var url = $('#my-tag-list').data("tag_url")
    $.get(url, dataType = 'json', function (data) {
        $('#my-tag-list').tags({
            tagData: [],
            suggestions: data,
            afterAddingTag: refresh_catalog,
            afterDeletingTag: refresh_catalog
        });
    })
}

function refresh_catalog(tag) {
    url = $('#catalog').data('url');
    data = {
        tags: $('#my-tag-list').tags().getTags(),
        page: pages.page
    };
    $.get(url, data, success, 'json');


    function success(data) {
        pages.total = parseInt(data.count);
        pages.page = parseInt(data.now);
        $('#page-selection').bootpag(pages);
        clean_catalog();
        data.goods.forEach(add_good);
    }

    function clean_catalog() {
        catalog = $('#catalog');
        catalog.empty();
    }

    function add_good(data, num, arr) {
        c = data.in_cart ? ' disabled': '';
        var good_inner = $('<div>', {'class': 'caption'})
            .append($('<a>', {'href': data.url})
                .append($('<h3>').text(data.title)))
            .append($('<p>').text(data.description))
            .append($('<h4>').text('Цена: ' + data.price + ' рублей'))
            .append($('<p>')
                .append($('<span>')
                    .attr({"class": 'btn btn-default add_to_cart'+c, 'data-good_id': data.id, 'role': "button"})
                    .text("Добавить в корзину")));
        var good = $('<div>', {'class': 'col-md-3 col-xs-6'})
            .append($('<div>', {'class': 'thumbnail'})
                .append($('<a>', {'href': data.url})
                    .append($('<img>', {'class': 'img-thumbnail', 'src': data.img_url})))
                .append(good_inner));

        $('#catalog').append(good);
    }
}

