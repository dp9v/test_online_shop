# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='good',
            name='properties',
            field=models.ManyToManyField(blank=True, verbose_name='Параметры', to='catalog.Property'),
        ),
        migrations.AlterField(
            model_name='good',
            name='tags',
            field=models.ManyToManyField(blank=True, verbose_name='Тэги', to='catalog.Tag'),
        ),
    ]
