# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20150822_2119'),
    ]

    operations = [
        migrations.RenameField(
            model_name='good',
            old_name='is_avtive',
            new_name='is_active',
        ),
        migrations.AlterField(
            model_name='property',
            name='sort_by',
            field=models.IntegerField(verbose_name='Поле для сортировки'),
        ),
    ]
