# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_auto_20150822_2324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='good',
            name='properties',
            field=models.ManyToManyField(related_name='goods', to='catalog.Property', verbose_name='Параметры', blank=True),
        ),
        migrations.AlterField(
            model_name='good',
            name='tags',
            field=models.ManyToManyField(related_name='goods', to='catalog.Tag', verbose_name='Тэги', blank=True),
        ),
    ]
