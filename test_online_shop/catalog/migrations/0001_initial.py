# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=50, verbose_name='Имя категории')),
                ('machine_id', models.SlugField(verbose_name='Машиночитаемое имя')),
                ('is_active', models.BooleanField(default=True, verbose_name='Отображать')),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
            },
        ),
        migrations.CreateModel(
            name='Good',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100, verbose_name='Наименование товара')),
                ('description', models.TextField(blank=True, verbose_name='Описание товара')),
                ('image', models.ImageField(blank=True, verbose_name='Изображение', upload_to='goods', null=True)),
                ('is_avtive', models.BooleanField(default=True, verbose_name='Отображать')),
                ('category', models.ForeignKey(verbose_name='Категория', related_name='goods', to='catalog.Category', on_delete=django.db.models.deletion.PROTECT)),
            ],
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100, verbose_name='Название свойства')),
                ('value', models.CharField(max_length=100, verbose_name='Значение')),
                ('sort_by', models.ImageField(verbose_name='Поле для сортировки', upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=50, verbose_name='Название тэга')),
            ],
        ),
        migrations.AddField(
            model_name='good',
            name='properties',
            field=models.ManyToManyField(to='catalog.Property', null=True, blank=True, verbose_name='Параметры'),
        ),
        migrations.AddField(
            model_name='good',
            name='tags',
            field=models.ManyToManyField(to='catalog.Tag', null=True, blank=True, verbose_name='Тэги'),
        ),
    ]
