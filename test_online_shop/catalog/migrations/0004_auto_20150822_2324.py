# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20150822_2210'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='good',
            options={'verbose_name': 'Товар', 'verbose_name_plural': 'Товары'},
        ),
        migrations.AlterModelOptions(
            name='property',
            options={'verbose_name': 'Свойство товара', 'verbose_name_plural': 'Свойства товаров'},
        ),
        migrations.AlterModelOptions(
            name='tag',
            options={'verbose_name': 'Тэг', 'verbose_name_plural': 'Тэги'},
        ),
        migrations.AddField(
            model_name='good',
            name='price',
            field=models.IntegerField(verbose_name='Цена', default=0),
        ),
        migrations.AlterField(
            model_name='category',
            name='machine_id',
            field=models.SlugField(verbose_name='Машиночитаемое имя', unique=True),
        ),
    ]
