from django.contrib import admin
from catalog.models import Category, Good, Tag, Property

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	pass

@admin.register(Good)
class GoodAdmin(admin.ModelAdmin):
	pass

@admin.register(Property)
class PropertyAdmin(admin.ModelAdmin):
	pass


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
	pass