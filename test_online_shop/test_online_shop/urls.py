
from django.conf.urls import include, url
from django.contrib import admin
from catalog import urls as catalog_urls
from clients import urls as clients_urls

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(catalog_urls)),
    url(r'^client/', include(clients_urls))
]
